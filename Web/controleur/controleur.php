<?php
/**
 * Created by PhpStorm.
 * User: Cyril.GOLDENSCHUE
 * Date: 27/05/2020
 * Time: 10:18
 */
//appel du fichier cryptage.php pour pouvoir avoir accès au fonction dans le fichier
require "extension/cryptage.php";
//appel du fichier model.php pour pouvoir avoir accès au fonction dans le fichier
require "modele/modele.php";
/**
 * @Description Permet d'accéder à l'accueil
 */
function accueil(){

    //$articles = getArticles();
    require 'vue/vue_Accueil.php';
}

/**
 * @Description Permet d'accéder à la page de connexion
 */
function login(){

    require 'vue/vue_Login.php';

}

/**
 * @Description Permet d'accéder à la page de connexion
 */
function recherche(){

    $Produits = getNameMateriel($_POST['RechercheName']);
    require 'vue/vue_Categorie.php';

}

/**
 * @Description Permet de se déconnecter
 */
function logout(){

    $_SESSION['Login'] = null;
    $_SESSION['TypeCompte'] = null;
    require 'vue/vue_Accueil.php';
}

/**
 * @Description Permet d'accéder à la page de connexion
 */
function register(){

    require 'vue/vue_Register.php';
}

/**
 * @Description Permet d'accéder à la page des catégories
 */
function categorie(){

    $Produits = getMateriel();
    require 'vue/vue_Categorie.php';

}

/**
 *@Description Permet d'accéder à la page du formulaire
 */
function formulaire(){

    $Produits = getMateriel();
    require 'vue/vue_Formulaire.php';
}

/**
 *@Description Permet d'accéder à la page de la console d'administration
 */
function consoleAdmin(){

    require 'vue/vue_ConsoleAdmin.php';
}

/**
 *@Description Permet d'accéder à la page de détails d'un article
 */
function details(){

    $Produits = getMateriel();
    require 'vue/vue_Details.php';
}

/**
 *@Description Permet d'emprunter du matériel
 */
function empruntMateriel(){

    $idCompte = getOneClientWEmail($_SESSION['Login']);
    $idUtilisateur = 0;
    foreach ($idCompte as $id){
        $idUtilisateur = $id['idComptes'];
    }
    if($_POST['fDateDebut'] >= $_POST['fDateFin']) {
        CreerEmprunt($_POST['fDateDebut'], $_POST['fDateFin'], $_GET['ID'], $idUtilisateur);
    }else{
        $Produits = getMateriel();
        $Erreur = "MauvaiseDate";
        require 'vue/vue_Formulaire.php';
    }
    $Produits = getMateriel();
    require 'vue/vue_Categorie.php';
}

/**
 *@Description Permet d'accéder à la liste des emprunts.
 */
function emprunt(string $Type){

    switch ($Type) {
        case 'Debut':
            $Resultats = getEmpruntCreer();
            break;
        case 'Cours':
            $Resultats = getEmpruntCours();
            break;
        case 'Fin':
            $Resultats = getEmpruntFin();
            break;
        case 'Refuser':
            $Resultats = getEmpruntRefuser();
            break;
    }
    $TypeEmprunt = $Type;
    require 'vue/vue_Emprunt.php';
}

/**
 *@Description Permet d'accéder à la liste des emprunts.
 */
function Rappel(){

    $Emprunts = getEmprunt($_GET['ID']);
    foreach ($Emprunts as $emprunt) {
        $destinataire = $emprunt['MailCompte'];
    }
    $Admins = getAdministrateur();
    $bcc = '';
    $i = 0;
    foreach ($Admins as $admin){
        if($i == 0){ $bcc = '?bcc='.$admin['MailCompte']; $i++; }else{
            $bcc = $bcc.';'.$admin['MailCompte'];;
        }
    }
    $objet = "Retard rendu"; // Objet du message
    header('Location:mailto:'.$destinataire.$bcc."&subject=".$objet."&body=Bonjour, %0D%0A %0D%0A La date de rendu de votre emprunt a été dépacée merci de ramener le matériel. %0D%0A %0D%0A Cordialement. %0D%0A %0D%0A Les Administrateurs du site d'emprunt d'electro.");

    $TypeEmprunt = 'Cours';
    $Resultats = getEmpruntCours();
    require 'vue/vue_Emprunt.php';
}

/**
 *@Description Permet d'accéder à la liste des emprunts.
 */
function Rendu(){

    $Emprunts = getEmprunt($_GET['ID']);
    foreach ($Emprunts as $emprunt) {
        $destinataire = $emprunt['MailCompte'];
    }
    $Admins = getAdministrateur();
    $bcc = '';
    $i = 0;
    foreach ($Admins as $admin){
        if($i == 0){ $bcc = '?bcc='.$admin['MailCompte']; $i++; }else{
            $bcc = $bcc.';'.$admin['MailCompte'];;
        }
    }
    $objet = "Retard rendu"; // Objet du message
    header('Location:mailto:'.$destinataire.$bcc."&subject=".$objet."&body=Bonjour, %0D%0A %0D%0A Le matériel emprunté à bien été rendu . %0D%0A %0D%0A Cordialement. %0D%0A %0D%0A Les Administrateurs du site d'emprunt d'electro.");


    ModifEmprunt($_GET['ID'], 'Fin');


    $Resultats = getEmpruntCours();
    $TypeEmprunt = 'Cours';
    //require 'vue/vue_Emprunt.php';
    header("Refresh:0;url=index.php?action=EmpruntCours");
}

/**
 *@Description Permet d'accéder à la liste des emprunts.
 */
function ValidationRefus(string $Type){

    switch ($Type) {
        case 'Valide':
            $Emprunts = getEmprunt($_GET['ID']);
            foreach ($Emprunts as $emprunt) {
                $destinataire = $emprunt['MailCompte'];
            }
            $Admins = getAdministrateur();
            $bcc = '';
            $i = 0;
            foreach ($Admins as $admin){
                if($i == 0){ $bcc = '?bcc='.$admin['MailCompte']; $i++; }else{
                    $bcc = $bcc.';'.$admin['MailCompte'];;
                }
            }
            $objet = "Demande d'emprunt"; // Objet du message
            header('Location:mailto:'.$destinataire.$bcc."&subject=".$objet."&body=Bonjour, %0D%0A %0D%0A Votre demande à bien été acceptées. %0D%0A %0D%0A Cordialement. %0D%0A %0D%0A Les Administrateurs du site d'emprunt d'electro.");
            ModifEmprunt($_GET['ID'], 'Valide');
            $Resultats = getEmpruntCreer();
            require 'vue/vue_Emprunt.php';
            break;
        case 'Refus':
            $Emprunts = getEmprunt($_GET['ID']);
            foreach ($Emprunts as $emprunt) {
                $destinataire = $emprunt['MailCompte'];
            }
            $Admins = getAdministrateur();
            $bcc = '';
            $i = 0;
            foreach ($Admins as $admin){
                if($i == 0){ $bcc = '?bcc='.$admin['MailCompte']; $i++; }else{
                    $bcc = $bcc.';'.$admin['MailCompte'];;
                }
            }
            $objet = "Demande d'emprunt"; // Objet du message
            header('Location:mailto:'.$destinataire.$bcc."&subject=".$objet."&body=Bonjour, %0D%0A %0D%0A Votre demande à bien été acceptées. %0D%0A %0D%0A Cordialement. %0D%0A %0D%0A Les Administrateurs du site d'emprunt d'electro.");

            ModifEmprunt($_GET['ID'], 'Refus');
            $Resultats = getEmpruntCreer();
            require 'vue/vue_Emprunt.php';
            break;
    }


}



/**
 *@Description Permet d'accéder à la page de gestion des clients, des composants, des catégories et du matériels
 */
function gestion(string $Type){

    $TypeGestion = $Type;
    switch ($Type){
        case 'Materiel':
            $resultats = getMateriel();
            require 'vue/vue_GestionMateriel.php';
            break;

        case 'Client':
            $resultats = getComptesWRole();
            require 'vue/vue_GestionClient.php';
            break;
    }


}

/**
 * @Description Permet de vérifier les informations d'inscription
 */
function VerifInscription(){
    //vérification pour l'enregistrement et la création d'un nouveau membre
if(filter_var($_POST['fEmail'], FILTER_VALIDATE_EMAIL)) {
    if ($_POST['fPassword'] == $_POST['fConfirmPassword']) {
        $Nom = $_POST['fName'];
        $Prenom = $_POST['fFirstName'];
        $Email = $_POST['fEmail'];
        $Pswd = Chiffrement::crypt($_POST['fPassword']);
        CreerMembre($Nom, $Prenom, $Email, $Pswd);
        $_SESSION['Login'] = $_POST['fEmail'];
        $_SESSION['TypeCompte'] = 1;
        require "vue/vue_Accueil.php";
    }else {
        $Erreur = "FauxMdp";
        require 'vue/vue_Register.php';
    }
}else {
    $Erreur = "FauxEmail";
    require 'vue/vue_Register.php';
}
}

/**
 * @Description Permet de vérifier les informations de connexion
 */
function VerifLogin(){
    //vérification pour l'enregistrement et la création d'un nouveau membre

    $resultats = getComptes();
    foreach ($resultats as $resultat) {
        $MDP = Chiffrement::decrypt($resultat['PasswordCompte']);
        if ($_POST['fEmail'] == $resultat['MailCompte']) {
            if ($_POST['fPassword'] == $MDP) {
                $_SESSION['Login'] = $_POST['fEmail'];
                $_SESSION['TypeCompte'] = $resultat['FkRoles'];
                require 'vue/vue_Accueil.php';
            } else {
                $Erreur = "FauxMdp";
                require 'vue/vue_Login.php';
            }
        }else {
            $Erreur = "FauxEmail";
            require 'vue/vue_Login.php';
        }
    }
}


/**
 * @Description Permet d'accéder à la page de création de matériel
 */
function PageAddMateriel(){

    require 'vue/vue_CreateUpdateMateriel.php';
}

/**
 * @Description Permet d'accéder à la page de création de client
 */
function PageAddClient(){

    require 'vue/vue_CreateUpdateClient.php';
}

/**
 * @Description Permet d'accéder à la page de modification de matériel
 */
function PageUpdateMateriel(){

    $resultats = getOneMateriel($_GET['ID']);
    require 'vue/vue_CreateUpdateMateriel.php';
}

/**
 * @Description Permet d'accéder à la page de modification de matériel
 */
function PageUpdateClient(){

    $resultats = getOneClient($_GET['ID']);
    require 'vue/vue_CreateUpdateClient.php';
}



/**
 * @Description Permet d'ajouter des données dans la table Materiel
 */
function AddMateriel(){

    $Categorie = $_POST['fCategorie'];
    $Modele = $_POST['fModele'];
    $Inventaire = $_POST['fInventaire'];
    $Serie = $_POST['fSerie'];
    CreerMateriel($Categorie, $Modele, $Inventaire, $Serie);

    $TypeGestion = "Materiel";
    $resultats = getMateriel();
    require "vue/vue_GestionMateriel.php";
}

/**
 * @Description Permet de vérifier les informations d'inscription
 */
function AddClient(){
    //vérification pour l'enregistrement et la création d'un nouveau membre
    if(filter_var($_POST['fEmail'], FILTER_VALIDATE_EMAIL)) {
        if ($_POST['fPassword'] == $_POST['fConfirmPassword']) {
            $Nom = $_POST['fName'];
            $Prenom = $_POST['fFirstName'];
            $Email = $_POST['fEmail'];
            $Pswd = Chiffrement::crypt($_POST['fPassword']);
            $TypeCompte = $_POST['fTypeCompte'];
            CreerMembreAdmin($Nom, $Prenom, $Email, $Pswd, $TypeCompte);
            $TypeGestion = 'Client';
            $resultats = getComptesWRole();
            require "vue/vue_GestionClient.php";
        } else {
            $Erreur = 'FauxMdp';
            require "vue/vue_CreateUpdateClient.php";
        }
    }else {
        $Erreur = 'FauxEmail';
        require "vue/vue_CreateUpdateClient.php";
    }
}



/**
 * @Description Permet de modifier des données dans la table Materiel
 */
function UpdateMateriel(){

    $Categorie = $_POST['fCategorie'];
    $Modele = $_POST['fModele'];
    $Inventaire = $_POST['fInventaire'];
    $Serie = $_POST['fSerie'];
    ModifMateriel($Categorie, $Modele, $Inventaire, $Serie, $_GET['ID']);

    $TypeGestion = "Materiel";
    $resultats = getMateriel();
    require "vue/vue_GestionMateriel.php";
}

/**
 * @Description Permet de modifier des données dans la table Materiel
 */
function UpdateClient()
{
if(filter_var($_POST['fEmail'], FILTER_VALIDATE_EMAIL)) {
    if ($_POST['fPassword'] == $_POST['fConfirmPassword']) {
        $Nom = $_POST['fName'];
        $Prenom = $_POST['fFirstName'];
        $Email = $_POST['fEmail'];
        if ($_POST['fPassword'] != '') {
            $Pswd = Chiffrement::crypt($_POST['fPassword']);
        }else{
            $Pswd = '';
        }
        $TypeCompte = $_POST['fTypeCompte'];

        ModifClient($Nom, $Prenom, $Email, $Pswd, $TypeCompte, $_GET['ID']);
        $TypeGestion = 'Client';
        $resultats = getComptesWRole();
        require "vue/vue_GestionClient.php";
    } else {
        $Erreur = 'FauxMdp';
        require "vue/vue_CreateUpdateClient.php";
    }
}else {
    $Erreur = 'FauxEmail';
    require "vue/vue_CreateUpdateClient.php";
}
}




/**
 * @Description Permet de supprimer des données dans la table Materiel
 */
function SuppMateriel(){

    DeleteMateriel($_GET['ID']);

    $TypeGestion = "Materiel";
    $resultats = getMateriel();
    require "vue/vue_GestionMateriel.php";
}

/**
 * @Description Permet de supprimer des données dans la table Comptes
 */
function SuppClient(){

    DeleteClient($_GET['ID']);

    $TypeGestion = "Client";
    $resultats = getComptesWRole();
    require "vue/vue_GestionClient.php";
}

/**
 * @Description Permet d'afficher la page d'erreur quand la page de destination n'existe pas
 */
function erreur(){
    //affichage de la page d'erreur
    require 'vue/vue_Erreur.php';
}
?>


