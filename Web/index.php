<?php
/**
 * Created by PhpStorm.
 * User: Cyril.GOLDENSCHUE
 * Date: 27.05.2020
 * Time: 14:30
 * Index.php : page de triage des actions reçues dans l'URL
 */

session_start();
require "controleur/controleur.php";

try {
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
        // Sélection de l'action passée par l'URL
        switch ($action) {
            case 'Accueil':
                accueil();
                break;

            case 'Login':
                login();
                break;

            case 'Categorie':
                categorie();
                break;

            case 'Recherche':
                recherche();
                break;

            case 'Formulaire':
                formulaire();
                break;

            case 'Emprunt':
                empruntMateriel();
                break;

            case 'Register':
                register();
                break;

            case 'Logout':
                logout();
                break;

            case 'ConfirmInscription';
                VerifInscription();
                break;

            case 'ConfirmConnexion';
                VerifLogin();
                break;

            case 'ConsoleAdmin';
                consoleAdmin();
                break;

            case 'GestionMateriel':
                gestion('Materiel');
                break;

            case 'GestionComposant':
                gestion('Composant');
                break;

            case 'GestionClient':
                gestion('Client');
                break;

            case 'GestionCategorie':
                gestion('Categorie');
                break;

            case 'EmpruntCours':
                emprunt('Cours');
                break;

            case 'EmpruntFin':
                emprunt('Fin');
                break;

            case 'EmpruntDebut':
                emprunt('Debut');
                break;

            case 'EmpruntRefuser':
                emprunt('Refuser');
                break;

            case 'ValideEmprunt':
                ValidationRefus('Valide');
                break;

            case 'RefusEmprunt':
                ValidationRefus('Refus');
                break;

            case 'RappelEmprunt':
                Rappel();
                break;

            case 'RendreEmprunt':
                Rendu();
                break;

            case 'Details';
                details();
                break;

            case 'CreationMateriel':
                AddMateriel();
                break;

            case 'UpdateMateriel':
                UpdateMateriel();
                break;

            case 'NouveauClient':
                AddClient();
                break;

            case 'UpdateClient':
                UpdateClient();
                break;

            case 'GestionNouveau':
                if (isset($_GET['TypeNew'])) {
                    $TypeNew = $_GET['TypeNew'];
                    // Sélection de l'action passée par l'URL
                    switch ($TypeNew) {
                        case 'Materiel':
                            PageAddMateriel();
                            break;
                        case 'Client':
                            PageAddClient();
                            break;
                    }
                }
                break;

            case 'GestionUpdate':
                if (isset($_GET['Type'])) {
                    $Type = $_GET['Type'];
                    // Sélection de l'action passée par l'URL
                    switch ($Type) {
                        case 'Materiel':
                            PageUpdateMateriel();
                            break;

                        case 'Client':
                            PageUpdateClient();
                            break;
                    }
                }
                break;

            case 'GestionDelete':
                if (isset($_GET['Type'])) {
                    $Type = $_GET['Type'];
                    // Sélection de l'action passée par l'URL
                    switch ($Type) {
                        case 'Materiel':
                            SuppMateriel();
                            break;

                        case 'Client':
                            SuppClient();
                            break;
                    }
                }
                break;

            default :
                throw new Exception( "action non valide" );
        }
    }
    else
        accueil();
}

catch (Exception $e)
{
    erreur ($e->getMessage());
}
