﻿<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Stock Electro</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="cyril Goldenschue">
<!--Less styles -->
   <!-- Other Less css file //different less files has different color scheam
	<link rel="stylesheet/less" type="text/css" href="themes/less/simplex.less">
	<link rel="stylesheet/less" type="text/css" href="themes/less/classified.less">
	<link rel="stylesheet/less" type="text/css" href="themes/less/amelia.less">  MOVE DOWN TO activate
	-->
	<!--<link rel="stylesheet/less" type="text/css" href="themes/less/bootshop.less">
	<script src="themes/js/less.js" type="text/javascript"></script> -->
	
<!-- Bootstrap style --> 
    <link id="callCss" rel="stylesheet" href="contenu/themes/bootshop/bootstrap.min.css" media="screen"/>
    <link href="contenu/themes/css/base.css" rel="stylesheet" media="screen"/>
<!-- Bootstrap style responsive -->	
	<link href="contenu/themes/css/bootstrap-responsive.min.css" rel="stylesheet"/>
	<link href="contenu/themes/css/font-awesome.css" rel="stylesheet" type="text/css">
<!-- Google-code-prettify -->	
	<link href="contenu/themes/js/google-code-prettify/prettify.css" rel="stylesheet"/>
<!-- fav and touch icons -->
    <link rel="shortcut icon" href="contenu/themes/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="contenu/themes/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="contenu/themes/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="contenu/themes/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="contenu/themes/images/ico/apple-touch-icon-57-precomposed.png">
	<style type="text/css" id="enject"></style>
  </head>
<body>
<div id="header">
    <div class="container">
        <!--<div id="welcomeLine" class="row">
            seulement pour les personnes connectées <div class="span6">Welcome!<strong> User</strong></div>
            <div class="span6">
                <div class="pull-right">
                    <a href="contenu/product_summary.html"><span class="">$</span></a>
                    <a href="contenu/product_summary.html"><span class="btn btn-mini btn-primary"><i class="icon-shopping-cart icon-white"></i> [ 3 ] Itemes in your cart </span> </a>
                </div>
            </div>
        </div>-->
<!-- Navbar ================================================== -->
<div id="logoArea" class="navbar">
<a id="smallScreen" data-target="#topMenu" data-toggle="collapse" class="btn btn-navbar">
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
</a>
  <div class="navbar-inner">
    <a class="brand" href="index.php"><img src="contenu/themes/images/LogoCPNV.png"/></a>
		<form class="form-inline navbar-search" method="post" <?php if (isset($_SESSION['Login'])){ ?>action="index.php?action=Recherche"<?php }else{ ?> action="index.php?action=Login" <?php } ?> >
		<input id="srchFld" class="srchTxt" name="RechercheName" type="text" />
		  <select class="srchTxt">
			<option>TOUT</option>
			<option>MATÉRIEL </option>
			<option>COMPOSANT </option>
		</select> 
		  <button type="submit" id="submitButton" class="btn btn-primary">Go</button>
    </form>
    <ul id="topMenu" class="nav pull-right">
        <li><a href="index.php?action=Accueil">Accueil</a></li>
        <li><a <?php if (!isset($_SESSION['Login'])){ ?> href="index.php?action=Login"<?php }else{ ?> href="index.php?action=Categorie" <?php } ?>>Catégorie</a>        </li>
       <!-- <li><a <?php if (!isset($_SESSION['Login'])){ ?> href="index.php?action=Login"<?php }else{ ?> href="index.php?action=Formulaire" <?php } ?>>Formulaire d'emprunt</a></li>-->
        <li><a href="index.php?action=Login" style="padding-right:0">
                    <?php if (!isset($_SESSION['Login'])){ ?>
        <li><a href="index.php?action=Login"><span class="btn btn-large btn-success"> Login</span></a></li>
        <?php }else{ if($_SESSION['TypeCompte'] == 2){?>
            <li><a href="index.php?action=ConsoleAdmin"><span class="btn btn-large btn-success"> Console d'administration</span></a></li>
            <?php } ?>
            <li><a href="index.php?action=Logout"><span class="btn btn-large btn-success">Logout</span></a></li>
        <?php } ?>

                </a></li>
    </ul>
    </div>
</div>
</div>
</div>
<!-- Header End====================================================================== -->
<!-- Page Content -->
<div class="contentArea">
    <div class="divPanel notop page-content">
        <div class="row-fluid">
            <div class="span12" id="divMain">
                <?=$contenu; ?>
            </div>
        </div>
    </div>
</div>
</div>
<!-- /.container -->

<div id="mainBody">
	<div class="container">
	<div class="row">


<!-- Footer ================================================================== -->
	<div  id="footerSection">
	<div class="container">
		<div class="row">
			<div class="span3">
				<h5>ACCOUNT</h5>
				<a href="contenu/login.html">YOUR ACCOUNT</a>
				<a href="contenu/login.html">PERSONAL INFORMATION</a>
				<a href="contenu/login.html">ADDRESSES</a>
				<a href="contenu/login.html">DISCOUNT</a>
				<a href="contenu/login.html">ORDER HISTORY</a>
			 </div>
			<div class="span3">
				<h5>INFORMATION</h5>
				<a href="contenu/contact.html">CONTACT</a>
				<a href="contenu/register.html">REGISTRATION</a>
				<a href="contenu/legal_notice.html">LEGAL NOTICE</a>
				<a href="contenu/tac.html">TERMS AND CONDITIONS</a>
				<a href="contenu/faq.html">FAQ</a>
			 </div>
			<div class="span3">
				<h5>OUR OFFERS</h5>
				<a href="#">NEW PRODUCTS</a> 
				<a href="#">TOP SELLERS</a>  
				<a href="contenu/special_offer.html">SPECIAL OFFERS</a>
				<a href="#">MANUFACTURERS</a> 
				<a href="#">SUPPLIERS</a> 
			 </div>
			<div id="socialMedia" class="span3 pull-right">
				<h5>SOCIAL MEDIA </h5>
				<a href="#"><img width="60" height="60" src="contenu/themes/images/facebook.png" title="facebook" alt="facebook"/></a>
				<a href="#"><img width="60" height="60" src="contenu/themes/images/twitter.png" title="twitter" alt="twitter"/></a>
				<a href="#"><img width="60" height="60" src="contenu/themes/images/youtube.png" title="youtube" alt="youtube"/></a>
			 </div> 
		 </div>
		<p class="pull-right">&copy; Bootshop</p>
	</div><!-- Container End -->
	</div>
<!-- Placed at the end of the document so the pages load faster ============================================= -->
	<script src="contenu/themes/js/jquery.js" type="text/javascript"></script>
	<script src="contenu/themes/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="contenu/themes/js/google-code-prettify/prettify.js"></script>
	
	<script src="contenu/themes/js/bootshop.js"></script>
    <script src="contenu/themes/js/jquery.lightbox-0.5.js"></script>
	
	<!-- Themes switcher section ============================================================================================= -->

<span id="themesBtn"></span>
</body>
</html>