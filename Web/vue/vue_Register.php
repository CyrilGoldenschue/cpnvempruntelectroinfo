<?php
/**
 * Created by PhpStorm.
 * User: Cyril.GOLDENSCHUE
 * Date: 27.05.2020
 * Time: 16:06
 */
//Page de register permettant de créer un nouveau compte.

ob_start();
$Page = 'Register';
$titre = "".$Page;
?>
<html><body>
    <article>
        <table style="border-collapse: separate; margin: auto">
            <td style=" width: 50%" align="center" valign="top">
                <h1><?= $Page; ?></h1>
                <?php
                if(isset($Erreur)){
                    if($Erreur == "FauxMdp"){
                        echo "Mot de passe incorrect";
                    }elseif($Erreur == "FauxLogin"){
                        echo "Login incorrect";
                    }
                }

                ?>
                <form method="post" action="index.php?action=ConfirmInscription">
                    <table class="table">
                        <tr>
                            <td width="50%">Email</td>
                            <td align="right"><input style="width: 100%"  type="text" name="fEmail" placeholder="Mettre votre email" required></td>
                        </tr>
                        <tr>
                            <td>Prénom</td>
                            <td align="right"><input style="width: 100%"  type="text" name="fFirstName" placeholder="Mettre votre prénom" required></td>
                        </tr>
                        <tr>
                            <td>Nom</td>
                            <td align="right"><input style="width: 100%"  type="text" name="fName" placeholder="Mettre votre nom" required></td>
                        </tr>
                        <tr>
                            <td>Mot de passe</td>
                            <td align="right"><input style="width: 100%" type="password" name="fPassword" oncopy="return false;" onpaste="return false;" oncut="return false;" placeholder="Mettre votre mot de passe" required></td>
                        </tr>
                        <tr>
                            <td>Confirmation du mot de passe</td>
                            <td align="right"><input style="width: 100%" type="password" name="fConfirmPassword" oncopy="return false;" onpaste="return false;" oncut="return false;" placeholder="Mettre votre mot de passe" required></td>
                        </tr>

                        <tr>
                            <td><input type="submit" value="Register"></td>
                            <td align="right"><input type="Reset"></td>
                        </tr>

                    </table>
                </form>
            </td>
        </table>
        <br>
    </article>
</body></html>

<?php

$contenu = ob_get_clean(); // stocks la page dans la variable
require "vue/gabarit.php";

