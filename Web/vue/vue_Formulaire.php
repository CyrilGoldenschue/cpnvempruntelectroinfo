<?php
/**
 * Created by PhpStorm.
 * User: Cyril.GOLDENSCHUE
 * Date: 28.05.2020
 * Time: 14:00
 */
//Page de catégorie qui permet de voir le matériel en stock ainsi que les composant avec un slider et les trois propositions les plus récentes

ob_start();
$Page = 'Formulaire';
$titre = "".$Page;
?>
    <div class="section">
        <div class="container" align="center">
            <h1><?= $Page;?></h1><br>
            <?php
            if(isset($Erreur)){
                if($Erreur == "MauvaiseDate"){
                    echo "La date du début est plus vieille que la date de fin";
                }
            }

            ?>
                <div>

                    <?php foreach ($Produits as $Produit){  if($_GET['ID'] == $Produit['idMateriels']){?>
                    <form method="post" action='index.php?action=Emprunt&ID=<?php echo $Produit['idMateriels'] ?>'>
                        <table style="margin: auto">
                            <tr>
                                <td>Catégorie : </td>
                                <td><input type="text" required value="<?php echo $Produit['CategorieMateriels'] ?>"></td>
                            </tr>
                            <tr>
                                <td>Modèle : </td>
                                <td><input type="text" required value="<?php echo $Produit['ModeleMateriels'] ?>"></td>
                            </tr>
                            <tr>
                                <td>Numéro d'inventaire : </td>
                                <td><input type="text" required value="<?php echo $Produit['NumInventaireMateriels'] ?>"></td>
                            </tr>
                            <tr>
                                <td>Numéro de série : </td>
                                <td><input type="text" required value="<?php echo $Produit['NumSerieMateriels'] ?>"></td>
                            </tr>
                            <tr>
                                <td>Date de début : </td>
                                <td><input type="date" required name="fDateDebut" min="<?php echo date("Y-m-d", strtotime("+0days")); ?>" ></td>
                            </tr>
                            <tr>
                                <td>Date de fin : </td>
                                <td><input type="date" required name="fDateFin"  min="<?php echo date("Y-m-d", strtotime("+0days")); ?>"></td>
                            </tr>
                            <tr>
                                <td colspan="2"><input style="width: 100%" type="submit" value="Emprunter"></td>
                            </tr>
                        </table>
                    </form>

                    <?php } } ?>
                    <br>
                </div>





        </div>
    </div>


<?php

$contenu = ob_get_clean(); // stocks la page dans la variable
require "vue/gabarit.php";



