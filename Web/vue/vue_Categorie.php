<?php
/**
 * Created by PhpStorm.
 * User: Cyril.GOLDENSCHUE
 * Date: 28.05.2020
 * Time: 11:00
 */
//Page de catégorie qui permet de voir le matériel en stock ainsi que les composant

ob_start();
$Page = 'Catégorie';
$titre = "".$Page;
?>
    <div class="section">
        <div class="container">
            <h1 align="center"><?= $Page;?></h1>

            <!-- Sidebar ================================================== -->
            <div id="sidebar" class="span3">
                <!--<div class="well well-small"><a id="myCart" href="contenu/product_summary.html"><img src="contenu/themes/images/ico-cart.png" alt="cart">3 Items in your cart  <span class="badge badge-warning pull-right">$155.00</span></a></div>-->
                <ul id="sideManu" class="nav nav-tabs nav-stacked">
                    <li class="subMenu open"><a> Matériel</a>
                        <ul>
                            <li><a class="active" href="contenu/products.html"><i class="icon-chevron-right"></i>Cameras (100) </a></li>
                            <li><a href="contenu/products.html"><i class="icon-chevron-right"></i>Computers, Tablets & laptop (30)</a></li>
                            <li><a href="contenu/products.html"><i class="icon-chevron-right"></i>Mobile Phone (80)</a></li>
                            <li><a href="contenu/products.html"><i class="icon-chevron-right"></i>Sound & Vision (15)</a></li>
                        </ul>
                    </li>
                    <li class="subMenu"><a> Composant </a>
                        <ul style="display:none">
                            <li><a href="contenu/products.html"><i class="icon-chevron-right"></i>Women's Clothing (45)</a></li>
                            <li><a href="contenu/products.html"><i class="icon-chevron-right"></i>Women's Shoes (8)</a></li>
                            <li><a href="contenu/products.html"><i class="icon-chevron-right"></i>Women's Hand Bags (5)</a></li>
                            <li><a href="contenu/products.html"><i class="icon-chevron-right"></i>Men's Clothings  (45)</a></li>
                            <li><a href="contenu/products.html"><i class="icon-chevron-right"></i>Men's Shoes (6)</a></li>
                            <li><a href="contenu/products.html"><i class="icon-chevron-right"></i>Kids Clothing (5)</a></li>
                            <li><a href="contenu/products.html"><i class="icon-chevron-right"></i>Kids Shoes (3)</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
                <table style="border-collapse: separate; width: 70% " align="left">
                    <?php $i = 0; foreach ($Produits as $Produit){ ?>
                    <?php if($i == 0) { ?><tr style="width: 100%; height:  40px; " ><?php } ?>
                        <td align="center" ">
                            <div class="product-body">
                                <input type="button" onclick="window.location.href='index.php?action=Details&ID=<?= $Produit['idMateriels'] ?>'" value="<?= $Produit['CategorieMateriels'] ?>">
                            </div>
                        </td>
                    <?php if($i == 5) { ?></tr><?php }; $i++; if($i == 5){ $i=0; }}?>
                </table>
        </div>
    </div>
<!-- Sidebar end=============================================== -->
<?php

$contenu = ob_get_clean(); // stocks la page dans la variable
require "vue/gabarit.php";

