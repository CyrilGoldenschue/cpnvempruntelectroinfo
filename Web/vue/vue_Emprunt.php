<?php
/**
 * Created by PhpStorm.
 * User: Cyril.GOLDENSCHUE
 * Date: 29.05.2020
 * Time: 10:37
 */
//Page de la console d'administration

ob_start();
$Page = "Liste des emprunts";
$titre = "".$Page;
?>
    <html><body>
    <article>
        <table style="border-collapse: separate; margin: auto">
            <td style=" width: 50%" align="center" valign="top">
                <h1><?php if(isset($TypeEmprunt)){
                        if($TypeEmprunt == "Cours"){
                            echo $Page." en cours";
                        }elseif($TypeEmprunt == "Debut"){
                            echo "Liste des demandes d'emprunts";
                        }elseif($TypeEmprunt == "Fin"){
                            echo $Page." finis";
                        }elseif($TypeEmprunt == "Refuser"){
                            echo $Page." refusés";
                        }
                    } ?></h1>
                <table class="table">
                    <tr>
                        <th>Mail</th>
                        <th>Catégorie</th>
                        <th>Modèle</th>
                        <th>N°inventaire</th>
                        <th>N°série</th>
                        <th>Date de début</th>
                        <th>Date de fin</th>
                        <?php if($TypeEmprunt != "Fin" and $TypeEmprunt != "Refuser"){ ?><th>Options</th><?php } ?>
                    </tr>
                    <?php foreach ($Resultats as $resultat) { ?>
                        <tr>
                            <td> <?php echo $resultat['MailCompte'] ?> </td>
                            <td> <?php echo $resultat['CategorieMateriels'] ?> </td>
                            <td> <?php echo $resultat['ModeleMateriels'] ?> </td>
                            <td> <?php echo $resultat['NumInventaireMateriels'] ?> </td>
                            <td> <?php echo $resultat['NumSerieMateriels'] ?> </td>
                            <td> <?php echo $resultat['DateDebut'] ?> </td>
                            <td> <?php echo $resultat['DateFin'] ?> </td>
                            <?php if($TypeEmprunt != "Fin" and $TypeEmprunt != "Refuser"){ ?>
                            <td align="right">

                               <?php if($TypeEmprunt == "Debut"){?> <input style="width: auto" type="button" value="Valider" onclick="window.location.href='index.php?action=ValideEmprunt&ID=<?php echo $resultat['idCommandes'] ?>'">
                                <input style="width: auto" type="button" value="Refuser" onclick="window.location.href='index.php?action=RefusEmprunt&ID=<?php echo $resultat['idCommandes'] ?>'">
                               <?php }else{ ?>
                                   <?php if(date("Y-m-d") > $resultat['DateFin'] and $resultat['idCommandes'] == "2" ){ ?><input style="width: auto" type="button" value="Rappel" onclick="window.location.href='index.php?action=RappelEmprunt&ID=<?php echo $resultat['idCommandes'] ?>'"><?php } ?>
                                <input style="width: auto" type="button" value="Rendre" onclick="window.location.href='index.php?action=RendreEmprunt&ID=<?php echo $resultat['idCommandes'] ?>'">
                               <?php } ?>
                            </td>
                            <?php } ?>
                        </tr>
                    <?php }?>
                </table>


            </td>
        </table>
        <br>
    </article>
    </body></html>

<?php

$contenu = ob_get_clean(); // stocks la page dans la variable
require "vue/gabarit.php";



