<?php
/**
 * Created by PhpStorm.
 * User: Cyril.GOLDENSCHUE
 * Date: 28.05.2020
 * Time: 10:41
 *
 */
//Page des détails qui permet de voir les informations d'un matériel ou d'un composant

ob_start();
$Page = 'Détails';
$titre = "".$Page;
?>
    <div class="section">
        <div class="container" align="center">
            <h1><?= $Page;?></h1><br>
            <?php foreach ($Produits as $Produit){  if($_GET['ID'] == $Produit['idMateriels']){?>
            <div>
                <table style="margin: auto">
                    <tr>
                        <td>Catégorie : </td>
                        <td><?php echo $Produit['CategorieMateriels'] ?></td>
                    </tr>
                    <tr>
                        <td>Modèle : </td>
                        <td><?php echo $Produit['ModeleMateriels'] ?></td>
                    </tr>
                    <tr>
                        <td>Numéro d'inventaire : </td>
                        <td><?php echo $Produit['NumInventaireMateriels'] ?></td>
                    </tr>
                    <tr>
                        <td>Numéro de série : </td>
                        <td><?php echo $Produit['NumSerieMateriels'] ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><input style="width: 100%" type="button" value="Emprunter" onclick="window.location.href='index.php?action=Formulaire&ID=<?php echo $Produit['idMateriels'] ?>'"></td>
                    </tr>
                </table>
            </div>
            <?php } } ?>





        </div>
    </div>


<?php

$contenu = ob_get_clean(); // stocks la page dans la variable
require "vue/gabarit.php";

