<?php
/**
 * Created by PhpStorm.
 * User: Cyril.GOLDENSCHUE
 * Date: 02.06.2020
 * Time: 15:54
 */
//Page de gestion pour les clients, le matériel, les composants et les catégoriels.

ob_start();
$Page = "Gestion";
$titre = "".$Page;
?>
    <html><body>
    <article>
        <table style="border-collapse: separate; margin: auto">
            <td style=" width: 50%" align="center" valign="top">
                <h1><?= $Page ?></h1><br>
                <input style="width: 100%" type="button" value="nouveau" onclick="window.location.href='index.php?action=GestionNouveau&TypeNew=<?php echo $TypeGestion ?>'"><br>
                <table class="table">

                    <tr>
                        <th>Nom</th>
                        <th>Prénom</th>
                        <th>Mail</th>
                        <th>Rôle</th>
                        <th>Options</th>
                    </tr>
                    <?php foreach ($resultats as $resultat) { ?>
                        <tr>
                            <td> <?php echo $resultat['NomCompte'] ?> </td>
                            <td> <?php echo $resultat['PrenomCompte'] ?> </td>
                            <td> <?php echo $resultat['MailCompte'] ?> </td>
                            <td> <?php echo $resultat['NomRole'] ?> </td>
                            <td align="right">
                                <input style="width: auto" type="button" value="Modifier" onclick="window.location.href='index.php?action=GestionUpdate&Type=Client&ID=<?php echo $resultat['idComptes'] ?>'">
                                <input style="width: auto" type="button" value="Supprimer" onclick="window.location.href='index.php?action=GestionDelete&Type=Client&ID=<?php echo $resultat['idComptes'] ?>'">
                            </td>
                        </tr>
                    <?php }?>
                </table>



            </td>
        </table>
    </article>
    </body></html>

<?php

$contenu = ob_get_clean(); // stocks la page dans la variable
require "vue/gabarit.php";



