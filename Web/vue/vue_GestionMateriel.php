<?php
/**
 * Created by PhpStorm.
 * User: Cyril.GOLDENSCHUE
 * Date: 01.06.2020
 * Time: 8:00
 */
//Page de gestion pour les clients, le matériel, les composants et les catégoriels.

ob_start();
$Page = "Gestion";
$titre = "".$Page;
$TypeProduit = $TypeGestion;
?>
    <html><body>
    <article>
        <table style="border-collapse: separate; margin: auto">
            <td style=" width: 50%" align="center" valign="top">
                <h1><?= $Page;
                if(isset($TypeGestion)){
                    if($TypeGestion == "Materiel"){
                        echo " du matériel";
                    }elseif($TypeGestion == "Categorie"){
                        echo " des catégories";
                    }elseif($TypeGestion == "Composant"){
                        echo " des composants";
                    }elseif($TypeGestion == "Client"){
                        echo " des clients";
                    }
                }?></h1><br><input style="width: 100%" type="button" value="nouveau" onclick="window.location.href='index.php?action=GestionNouveau&TypeNew=<?php echo $TypeGestion ?>'"><br>
                <table class="table">

                    <tr>
                        <th>Catégorie</th>
                        <th>Modèle</th>
                        <th>N°Inventaire</th>
                        <th>N°Série</th>
                        <th>Options</th>
                    </tr>
                    <?php foreach ($resultats as $resultat) { ?>
                    <tr>
                        <td> <?php echo $resultat['CategorieMateriels'] ?> </td>
                        <td> <?php echo $resultat['ModeleMateriels'] ?> </td>
                        <td> <?php echo $resultat['NumInventaireMateriels'] ?> </td>
                        <td> <?php echo $resultat['NumSerieMateriels'] ?> </td>
                        <td align="right">
                            <input style="width: auto" type="button" value="Modifier" onclick="window.location.href='index.php?action=GestionUpdate&Type=<?php echo $TypeProduit ?>&ID=<?php echo $resultat['idMateriels'] ?>'">
                            <input style="width: auto" type="button" value="Supprimer" onclick="window.location.href='index.php?action=GestionDelete&Type=<?php echo $TypeProduit ?>&ID=<?php echo $resultat['idMateriels'] ?>'">
                        </td>
                    </tr>
                <?php }?>
                </table>



            </td>
        </table>
    </article>
    </body></html>

<?php

$contenu = ob_get_clean(); // stocks la page dans la variable
require "vue/gabarit.php";



