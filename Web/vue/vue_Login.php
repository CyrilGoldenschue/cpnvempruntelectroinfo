<?php
/**
 * Created by PhpStorm.
 * User: Cyril.GOLDENSCHUE
 * Date: 27.05.2020
 * Time: 16:06
 */
//Page d'accueil avec un slider et les trois propositions les plus récentes

ob_start();
$Page = 'Login';
$titre = "".$Page;
?>
<html><body>
    <article>
        <table style="border-collapse: separate; margin: auto">
            <td style=" width: 50%" align="center" valign="top">
                <h1><?= $Page; ?></h1>
                <?php
                if(isset($Erreur)){
                    if($Erreur == "FauxMdp"){
                        echo "Mot de passe incorrect";
                    }elseif($Erreur == "FauxEmail"){
                        echo "Email incorrect";
                    }
                }

                ?>
                <form method="post" action="index.php?action=ConfirmConnexion">
                    <table class="table">
                        <tr>
                            <td width="50%">Email</td>
                            <td align="right"><input style="width: 100%"  type="text" name="fEmail" placeholder="Mettre votre email" required></td>
                        </tr>
                        <tr>
                            <td>Mot de passe</td>
                            <td align="right"><input style="width: 100%" type="password" name="fPassword" placeholder="Mettre votre mot de passe" required></td>
                        </tr>

                        <tr>
                            <td><input type="submit" value="Login"></td>
                            <td align="right"><input type="reset"></td>
                        </tr>
                        <tr>
                            <td colspan="2"><a href="index.php?action=Register">Vous n'avez pas encore de compte ?</a></td>
                        </tr>
                    </table>
                </form>
            </td>
        </table>
        <br>
    </article>
</body></html>

<?php

$contenu = ob_get_clean(); // stocks la page dans la variable
require "vue/gabarit.php";

