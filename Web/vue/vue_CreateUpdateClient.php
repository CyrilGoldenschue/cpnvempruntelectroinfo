<?php
/**
 * Created by PhpStorm.
 * User: Cyril.GOLDENSCHUE
 * Date: 03.06.2020
 * Time: 9:00
 */
//Page de register permettant de créer un nouveau compte.

ob_start();
$Page = 'Création de client';
$titre = "".$Page;
?>
    <html><body>
    <article>
        <table style="border-collapse: separate; margin: auto">
            <td style=" width: 50%" align="center" valign="top">
            <?php if(isset($resultats)) foreach ($resultats as $resultat) { ?>
                <h1>Modification du client</h1>
                <?php
                if(isset($Erreur)){
                    if($Erreur == "FauxMdp"){
                        echo "Mot de passe incorrect";
                    }elseif($Erreur == "FauxEmail"){
                        echo "Email incorrect";
                    }
                }

                ?>

                <form method="post" action="index.php?action=UpdateClient&ID=<?php echo $_GET['ID'] ?>">
                    <table class="table">
                        <tr>
                            <td width="50%">Email</td>
                            <td align="right"><input style="width: 100%"  type="text" name="fEmail" placeholder="Mettre l'email" value="<?php echo $resultat['MailCompte'] ?>" required></td>
                        </tr>
                        <tr>
                            <td>Prénom</td>
                            <td align="right"><input style="width: 100%"  type="text" name="fFirstName" placeholder="Mettre le prénom"  value="<?php echo $resultat['PrenomCompte'] ?>" required></td>
                        </tr>
                        <tr>
                            <td>Nom</td>
                            <td align="right"><input style="width: 100%"  type="text" name="fName" placeholder="Mettre le nom" value="<?php echo $resultat['NomCompte'] ?>" required></td>
                        </tr>
                        <tr>
                            <td>Mot de passe</td>
                            <td align="right"><input style="width: 100%" type="password" name="fPassword" oncopy="return false;" onpaste="return false;" oncut="return false;" placeholder="Mettre le mot de passe"></td>
                        </tr>
                        <tr>
                            <td>Confirmation du mot de passe</td>
                            <td align="right"><input style="width: 100%" type="password" name="fConfirmPassword" oncopy="return false;" onpaste="return false;" oncut="return false;" placeholder="Confirmer le mot de passe"></td>
                        </tr>
                        <tr>
                            <td>Type de compte</td>
                            <td align="right">
                                <select name="fTypeCompte" id="TypeSelect">
                                    <option value="Client" <?php if($resultat['NomRole'] == 'Client'){ ?> selected <?php } ?>>Client</option>
                                    <option value="Administration" <?php if($resultat['NomRole'] == 'Administration'){ ?> selected <?php } ?>>Administration</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><input type="submit" value="Valider"></td>
                            <td align="right"><input type="reset"></td>
                        </tr>
                    </table>
                </form>
            <?php }else{ $Page = 'Création de client' ?>
                <h1><?= $Page; ?></h1>
                <?php
                if(isset($Erreur)){
                    if($Erreur == "FauxMdp"){
                        echo "Mot de passe incorrect";
                    }elseif($Erreur == "FauxEmail"){
                        echo "Email incorrect";
                    }
                }

                ?>
                <form method="post" action="index.php?action=NouveauClient">
                    <table class="table">
                        <tr>
                            <td width="50%">Email</td>
                            <td align="right"><input style="width: 100%"  type="text" name="fEmail" placeholder="Mettre l'email" required></td>
                        </tr>
                        <tr>
                            <td>Prénom</td>
                            <td align="right"><input style="width: 100%"  type="text" name="fFirstName" placeholder="Mettre le prénom" required></td>
                        </tr>
                        <tr>
                            <td>Nom</td>
                            <td align="right"><input style="width: 100%"  type="text" name="fName" placeholder="Mettre le nom" required></td>
                        </tr>
                        <tr>
                            <td>Mot de passe</td>
                            <td align="right"><input style="width: 100%" type="password" name="fPassword" oncopy="return false;" onpaste="return false;" oncut="return false;" placeholder="Mettre le mot de passe" required></td>
                        </tr>
                        <tr>
                            <td>Confirmation du mot de passe</td>
                            <td align="right"><input style="width: 100%" type="password" name="fConfirmPassword" oncopy="return false;" onpaste="return false;" oncut="return false;" placeholder="Confirmer le mot de passe" required></td>
                        </tr>
                        <tr>
                            <td>Type de compte</td>
                            <td align="right">
                                <select name="fTypeCompte" id="TypeSelect">
                                    <option value="Client">Client</option>
                                    <option value="Administration">Administration</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td><input type="submit" value="Valider"></td>
                            <td align="right"><input type="reset"></td>
                        </tr>

                    </table>
                    <?php } ?>
                </form>
            </td>
        </table>
        <br>
    </article>
    </body></html>

<?php

$contenu = ob_get_clean(); // stocks la page dans la variable
require "vue/gabarit.php";

