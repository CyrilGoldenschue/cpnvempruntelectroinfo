<?php
/**
 * Created by PhpStorm.
 * User: Cyril.GOLDENSCHUE
 * Date: 29.05.2020
 * Time: 10:37
 */
//Page de la console d'administration

ob_start();
$Page = "Console d'administration";
$titre = "".$Page;
?>
    <html><body>
    <article>
        <table style="border-collapse: separate; margin: auto">
            <td style=" width: 50%" align="center" valign="top">
                <h1><?= $Page; ?></h1>
                <table class="table">
                    <tr align="middle">
                        <td width="100%" colspan="2"><input style="width: 100%"  type="button" value="Liste des demandes d'emprunt" onclick="window.location.href='index.php?action=EmpruntDebut'"></td>
                    </tr>
                    <tr align="middle">
                        <td width="100%" colspan="2"><input style="width: 100%" type="button"  value="Liste des emprunts en cours" onclick="window.location.href='index.php?action=EmpruntCours'"></td>
                    </tr>
                    <tr>
                        <td width="100%" colspan="2"><input style="width: 100%" type="button"  value="Liste des emprunts fini" onclick="window.location.href='index.php?action=EmpruntFin'"></td>
                    </tr>
                    <tr>
                        <td width="100%" colspan="2"><input style="width: 100%" type="button"  value="Liste des emprunts refuser" onclick="window.location.href='index.php?action=EmpruntRefuser'"></td>
                    </tr>
                    <tr>
                        <!--<td width="50%"><input style="width: 100%" type="button" value="Gestion des composants" onclick="window.location.href='index.php?action=GestionComposant'"></td>-->
                        <td width="50%"><input style="width: 100%" type="button" value="Gestion du matériels" onclick="window.location.href='index.php?action=GestionMateriel'"></td>
                    </tr>
                    <tr>
                        <!--<td width="50%"><input style="width: 100%" type="button" value="Gestion des catégories" onclick="window.location.href='index.php?action=GestionCategorie'"></td>-->
                        <td width="50%"><input style="width: 100%"  type="button" value="Gestion des clients" onclick="window.location.href='index.php?action=GestionClient'"></td>
                    </tr>
                </table>
            </td>
        </table>
        <br>
    </article>
    </body></html>

<?php

$contenu = ob_get_clean(); // stocks la page dans la variable
require "vue/gabarit.php";



