<?php
/**
 * Created by PhpStorm.
 * User: Cyril.GOLDENSCHUE
 * Date: 27.05.2020
 * Time: 16:06
 */
//Page d'accueil avec un slider et les trois propositions les plus récentes

ob_start();
$Page = 'Modification de matériel';
$titre = "".$Page;
?>
    <html><body>
    <article>
        <table style="border-collapse: separate; margin: auto">
            <td style=" width: 50%" align="center" valign="top">
                <?php if(isset($resultats)) foreach ($resultats as $resultat) { ?>
                <h1><?= $Page; ?></h1>

                <form method="post" action="index.php?action=UpdateMateriel&ID=<?php echo $_GET['ID'] ?>">
                    <table class="table">
                        <tr>
                            <td width="50%">Catégorie</td>
                            <td align="right"><input style="width: 100%"  type="text" name="fCategorie" placeholder="Mettre la catégorie" value="<?php echo $resultat['CategorieMateriels'] ?>" required></td>
                        </tr>
                        <tr>
                            <td>Modèle</td>
                            <td align="right"><input style="width: 100%" type="text" name="fModele" placeholder="Mettre le modèle" value="<?php echo $resultat['ModeleMateriels'] ?>" required></td>
                        </tr>
                        <tr>
                            <td>N° Inventaire</td>
                            <td align="right"><input style="width: 100%" type="text" name="fInventaire" placeholder="Mettre le numéro d'inventaire" value="<?php echo $resultat['NumInventaireMateriels'] ?>" required></td>
                        </tr>
                        <tr>
                            <td>N° Série</td>
                            <td align="right"><input style="width: 100%" type="text" name="fSerie" placeholder="Mettre le numéro de série" value="<?php echo $resultat['NumSerieMateriels'] ?>" required></td>
                        </tr>

                        <tr>
                            <td><input type="submit" value="Valider"></td>
                            <td align="right"><input type="reset"></td>
                        </tr>
                    </table>
                </form>
                <?php }else{ $Page = 'Création de matériel' ?>
                <h1><?= $Page; ?></h1>
                <form method="post" action="index.php?action=CreationMateriel">
                    <table class="table">
                        <tr>
                            <td width="50%">Catégorie</td>
                            <td align="right"><input style="width: 100%"  type="text" name="fCategorie" placeholder="Mettre la catégorie" required></td>
                        </tr>
                        <tr>
                            <td>Modèle</td>
                            <td align="right"><input style="width: 100%" type="text" name="fModele" placeholder="Mettre le modèle" required></td>
                        </tr>
                        <tr>
                            <td>N° Inventaire</td>
                            <td align="right"><input style="width: 100%" type="text" name="fInventaire" placeholder="Mettre le numéro d'inventaire" required></td>
                        </tr>
                        <tr>
                            <td>N° Série</td>
                            <td align="right"><input style="width: 100%" type="text" name="fSerie" placeholder="Mettre le numéro de série" required></td>
                        </tr>

                        <tr>
                            <td><input type="submit" value="Valider"></td>
                            <td align="right"><input type="reset"></td>
                        </tr>
                    </table>
                </form>
                <?php } ?>
            </td>
        </table>
        <br>
    </article>
    </body></html>

<?php

$contenu = ob_get_clean(); // stocks la page dans la variable
require "vue/gabarit.php";

