<?php

// getBD()
// Date de création : 27/05/2020
// Fonction : connexion avec le serveur : instancie et renvoie l'objet PDO
// Sortie : $connexion
/**
 * @Description Connexion à la base de donnée
 * @return PDO
 */
function getBD()
{
    // connexion au serveur MySQL et à la BD
    $connexion = new PDO('mysql:host=localhost; dbname=mld','root', 'CpnvF0rever');
    // permet d'avoir plus de détails sur les erreurs retournées
    $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $connexion;
}

/**
 * @Description Accès à la table comptes
 * @return PDOStatement
 */
function getComptes()
{
    // connexion à la BD StockElectro
    $connexion = getBD();

    // Création de la string pour la requête
    $requete = "SELECT * FROM comptes";
    // Exécution de la requete
    $resultats = $connexion->query($requete);

    return $resultats;
}

/**
 * @Description Accès à la table comptes avec les roles
 * @return PDOStatement
 */
function getComptesWRole()
{
    // connexion à la BD StockElectro
    $connexion = getBD();

    // Création de la string pour la requête
    $requete = "SELECT * FROM comptes INNER JOIN roles ON comptes.FkRoles = roles.idRoles";
    // Exécution de la requete
    $resultats = $connexion->query($requete);

    return $resultats;
}


/**
 * @Description Accès à la table Matériels
 * @return PDOStatement
 */
function getMateriel()
{
    // connexion à la BD StockElectro
    $connexion = getBD();

    // Création de la string pour la requête
    $requete = "SELECT * FROM materiels";
    // Exécution de la requete
    $resultats = $connexion->query($requete);

    return $resultats;
}

/**
 * @Description Accès à la table Matériels pour rechercher un matériel par rapport à l'id
 * @return PDOStatement
 */
function getOneMateriel(int $Id)
{
    // connexion à la BD StockElectro
    $connexion = getBD();

    // Création de la string pour la requête
    $requete = "SELECT * FROM materiels WHERE idMateriels = ".$Id;
    // Exécution de la requete
    $resultats = $connexion->query($requete);

    return $resultats;
}

/**
 * @Description Accès à la table Matériels pour rechercher un matériel par rapport à l'id
 * @return PDOStatement
 */
function getNameMateriel(string $name)
{
    // connexion à la BD StockElectro
    $connexion = getBD();

    // Création de la string pour la requête
    $requete = "SELECT * FROM materiels WHERE CategorieMateriels = '".$name."'";
    // Exécution de la requete
    $resultats = $connexion->query($requete);

    return $resultats;
}

/**
 * @Description Accès à la table Compte pour rechercher un compte par rapport à l'id
 * @return PDOStatement
 * @param $Id
 */
function getOneClient(int $Id)
{
    // connexion à la BD StockElectro
    $connexion = getBD();

    // Création de la string pour la requête
    $requete = "SELECT * FROM comptes INNER JOIN roles ON comptes.FkRoles = roles.idRoles WHERE idComptes = ".$Id;
    // Exécution de la requete
    $resultats = $connexion->query($requete);

    return $resultats;
}

/**
 * @Description Accès à la table Compte pour rechercher un compte par rapport à l'email
 * @return PDOStatement
 * @param $Id
 */
function getOneClientWEmail(string $Email)
{
    // connexion à la BD StockElectro
    $connexion = getBD();

    // Création de la string pour la requête
    $requete = "SELECT idComptes FROM comptes WHERE MailCompte = '".$Email."'";
    // Exécution de la requete
    $resultats = $connexion->query($requete);

    return $resultats;
}

/**
 * @Description Accès à la table Compte pour rechercher un compte par rapport à l'id
 * @return PDOStatement
 */
function getAdministrateur()
{
    // connexion à la BD StockElectro
    $connexion = getBD();

    // Création de la string pour la requête
    $requete = "SELECT * FROM comptes WHERE FkRoles = 2";
    // Exécution de la requete
    $resultats = $connexion->query($requete);

    return $resultats;
}

/**
 * @Description Accès à la table Emprunts pour rechercher un compte par rapport au statut Créer
 * @return PDOStatement
 */
function getEmpruntCreer()
{
    // connexion à la BD StockElectro
    $connexion = getBD();

    // Création de la string pour la requête
    $requete = "SELECT * FROM emprunts INNER JOIN comptes ON comptes.idComptes = emprunts.Comptes_idComptes INNER JOIN statuts ON statuts.idTypeCommandes = emprunts.Statuts_idTypeCommandes INNER JOIN materiels ON materiels.idMateriels = emprunts.Materiels_idMateriels WHERE Statuts_idTypeCommandes = 1";
    // Exécution de la requete
    $resultats = $connexion->query($requete);

    return $resultats;
}

/**
 * @Description Accès à la table Emprunts pour rechercher une demande par rapport à son identifiant
 * @return PDOStatement
 */
function getEmprunt(int $Id)
{
    // connexion à la BD StockElectro
    $connexion = getBD();

    // Création de la string pour la requête
    $requete = "SELECT * FROM emprunts INNER JOIN comptes ON comptes.idComptes = emprunts.Comptes_idComptes INNER JOIN statuts ON statuts.idTypeCommandes = emprunts.Statuts_idTypeCommandes INNER JOIN materiels ON materiels.idMateriels = emprunts.Materiels_idMateriels WHERE idCommandes = ".$Id;
    // Exécution de la requete
    $resultats = $connexion->query($requete);

    return $resultats;
}

/**
 * @Description Accès à la table Emprunts pour rechercher un compte par rapport au statut En Cours
 * @return PDOStatement
 */
function getEmpruntCours()
{
    // connexion à la BD StockElectro
    $connexion = getBD();

    // Création de la string pour la requête
    $requete = "SELECT * FROM emprunts INNER JOIN comptes ON comptes.idComptes = emprunts.Comptes_idComptes INNER JOIN statuts ON statuts.idTypeCommandes = emprunts.Statuts_idTypeCommandes INNER JOIN materiels ON materiels.idMateriels = emprunts.Materiels_idMateriels WHERE Statuts_idTypeCommandes = 2";
    // Exécution de la requete
    $resultats = $connexion->query($requete);

    return $resultats;
}

/**
 * @Description Accès à la table Emprunts pour rechercher un compte par rapport au statut Fin
 * @return PDOStatement
 */
function getEmpruntFin()
{
    // connexion à la BD StockElectro
    $connexion = getBD();

    // Création de la string pour la requête
    $requete = "SELECT * FROM emprunts INNER JOIN comptes ON comptes.idComptes = emprunts.Comptes_idComptes INNER JOIN statuts ON statuts.idTypeCommandes = emprunts.Statuts_idTypeCommandes INNER JOIN materiels ON materiels.idMateriels = emprunts.Materiels_idMateriels WHERE Statuts_idTypeCommandes = 3";
    // Exécution de la requete
    $resultats = $connexion->query($requete);

    return $resultats;
}

/**
 * @Description Accès à la table Emprunts pour rechercher un compte par rapport au statut Refuser
 * @return PDOStatement
 */
function getEmpruntRefuser()
{
    // connexion à la BD StockElectro
    $connexion = getBD();

    // Création de la string pour la requête
    $requete = "SELECT * FROM emprunts INNER JOIN comptes ON comptes.idComptes = emprunts.Comptes_idComptes INNER JOIN statuts ON statuts.idTypeCommandes = emprunts.Statuts_idTypeCommandes INNER JOIN materiels ON materiels.idMateriels = emprunts.Materiels_idMateriels WHERE Statuts_idTypeCommandes = 4";
    // Exécution de la requete
    $resultats = $connexion->query($requete);

    return $resultats;
}




/**
 * @Description Insertion d'un membre dans la table Comptes
 * @param $Nom
 * @param $Prenom
 * @param $Email
 * @param $Pswd
 */
function CreerMembre($Nom, $Prenom, $Email, $Pswd){

    //Création d'utilisateur

    $TypeCompte = 1;

    $connexion = getBD();
    $req = "INSERT INTO Comptes (NomCompte, PrenomCompte, MailCompte, PasswordCompte, FkRoles) VALUES ('$Nom', '$Prenom','$Email', '$Pswd', '$TypeCompte')";

    $connexion->exec($req);
}

/**
 * @Description Insertion d'un membre dans la table Comptes en tant qu'admin
 * @param $Nom
 * @param $Prenom
 * @param $Email
 * @param $Pswd
 * @param $TypeCompte
 */
function CreerMembreAdmin($Nom, $Prenom, $Email, $Pswd, $TypeCompte)
{

//Création d'utilisateur

    if($TypeCompte == 'Client'){
        $Compte = 1;
    }else{
        $Compte = 2;
    }

    $connexion = getBD();
    $req = "INSERT INTO Comptes (NomCompte, PrenomCompte, MailCompte, PasswordCompte, FkRoles) VALUES ('$Nom', '$Prenom','$Email', '$Pswd', '$Compte')";

    $connexion->exec($req);
}

/**
 * @Description Insertion d'un produit dans la table Materiels
 * @param $Categorie
 * @param $Modele
 * @param $NumInventaire
 * @param $NumSerie
 */
function CreerMateriel($Categorie, $Modele, $NumInventaire, $NumSerie){

    //Création d'un matériel

    $connexion = getBD();
    $req = "INSERT INTO Materiels (CategorieMateriels, ModeleMateriels, NumInventaireMateriels, NumSerieMateriels) VALUES ('$Categorie', '$Modele','$NumInventaire', '$NumSerie')";

    $connexion->exec($req);
}

/**
 * @Description Insertion d'un membre dans la table Comptes
 * @param $Nom
 * @param $Prenom
 * @param $Email
 * @param $Pswd
 */
function CreerEmprunt($DateDebut, $DateFin, $idMateriel, $idCompte){

    //Création d'utilisateur

    $Status = 1;
    $DateDebut = date("Y-m-d", strtotime($DateDebut));
    $DateFin = date("Y-m-d", strtotime($DateFin));
    $Test   = $idCompte;
    $connexion = getBD();
    $req = "INSERT INTO emprunts (Comptes_idComptes, Statuts_idTypeCommandes, Materiels_idMateriels, DateDebut, DateFin) VALUES ('$idCompte', '$Status','$idMateriel', '$DateDebut', '$DateFin')";

    $connexion->exec($req);
}



/**
 * @Description Insertion d'un produit dans la table Materiels
 * @param $Categorie
 * @param $Modele
 * @param $NumInventaire
 * @param $NumSerie
 * @param $Id
 */
function ModifMateriel($Categorie, $Modele, $NumInventaire, $NumSerie, $Id){

    //Création d'un matériel

    $connexion = getBD();
    $req = "UPDATE Comptes SET CategorieMateriels='$Categorie', ModeleMateriels='$Modele', NumInventaireMateriels='$NumInventaire', NumSerieMateriels='$NumSerie' WHERE idMateriels='$Id'";

    $connexion->exec($req);
}

/**
 * @Description Modification du status d'un emprunt
 * @param $Statut
 * @param $Id
 */
function ModifEmprunt($Id, $Statut){

    //Création d'un matériel
    if($Statut == "Valide"){
        $idStatus = 2;
    }elseif($Statut == "Refus"){
        $idStatus = 4;
    }elseif($Statut == "Fin"){
        $idStatus = 3;
    }

    $connexion = getBD();
    $req = "UPDATE emprunts SET Statuts_idTypeCommandes='$idStatus' WHERE idCommandes='$Id'";

    $connexion->exec($req);
}

/**
 * @Description Insertion d'un produit dans la table Materiels
 * @param $Categorie
 * @param $Modele
 * @param $NumInventaire
 * @param $NumSerie
 * @param $Id
 */
function ModifClient($Nom, $Prenom, $Email, $Pswd, $TypeCompte, $Id){

    //Création d'un matériel

    if($TypeCompte == 'Client'){
        $Compte = 1;
    }else{
        $Compte = 2;
    }

    $connexion = getBD();
    if($Pswd == '') {
        $req = "UPDATE Comptes SET NomCompte='$Nom', PrenomCompte='$Prenom', MailCompte='$Email', FkRoles = '$Compte' WHERE idComptes='$Id'";
    }else {
        $req = "UPDATE Comptes SET NomCompte='$Nom', PrenomCompte='$Prenom', MailCompte='$Email', PasswordCompte='$Pswd', FkRoles = '$Compte' WHERE idComptes='$Id'";
    }

    $connexion->exec($req);
}



/**
 * @Description Suppression d'un produit dans la table Materiels
 * @param $Id
 */
function DeleteMateriel($Id){

    //Création d'un matériel

    $connexion = getBD();
    $req = "DELETE FROM Materiels WHERE idMateriels='$Id'";

    $connexion->exec($req);
}

/**
 * @Description Suppresion d'un produit dans la table Comptes
 * @param $Id
 */
function DeleteClient($Id){

    //Création d'un matériel

    $connexion = getBD();
    $req = "DELETE FROM Comptes WHERE idComptes='$Id'";

    $connexion->exec($req);
}










/**
 * @Description Accès à la table produits qui est lié à la table typeproduit
 * @return PDOStatement
 */
function getArticles()
{
    // connexion à la BD octavia
    $connexion = getBD();

    // Création de la string pour la requête
    $requete = "SELECT * FROM produits INNER JOIN typeproduit ON produits.FKTypeProduit = typeproduit.idTypeProduit";
    // Exécution de la requete
    $resultats = $connexion->query($requete);

    return $resultats;
}

/**
 * @Description Accès à la table villes
 * @return PDOStatement
 */
function getVilles(){
    // connexion à la BD octavia
    $connexion = getBD();

    // Création de la string pour la requête
    $requete = "SELECT * FROM villes";
    // Exécution de la requete
    $resultats = $connexion->query($requete);

    return $resultats;
}

/**
 * @Description Accès à la table villeparclient qui est lié à la table villes
 * @return PDOStatement
 */
function getVillesParClient(){

    // connexion à la BD octavia
    $connexion = getBD();

    // Création de la string pour la requête
    $requete = "SELECT * FROM villeparclient INNER JOIN villes ON villeparclient.FKVilles = villes.idVilles";
    // Exécution de la requete
    $resultats = $connexion->query($requete);

    return $resultats;
}

/**
 * @Description Accès à la table commandes
 * @return PDOStatement
 */
function getCommandes(){
    // connexion à la BD octavia
    $connexion = getBD();

    // Création de la string pour la requête
    $requete = "SELECT * FROM commandes";
    // Exécution de la requete
    $resultats = $connexion->query($requete);

    return $resultats;
}

/**
 * @Description Accès à la table produitparcommande lié à la table commandes
 * @return PDOStatement
 */
function getCommandeEtProduit(){
    // connexion à la BD octavia
$connexion = getBD();

// Création de la string pour la requête
$requete = "SELECT * FROM produitparcommande INNER JOIN commandes ON produitparcommande.FKCommande = commandes.idCommandes INNER JOIN produits ON produitparcommande.FKProduit = produits.idProduits";
// Exécution de la requete
$resultats = $connexion->query($requete);

return $resultats;
}



/**
 * @Description Insertion d'un vendeur dans la table membres
 * @param $Login
 * @param $Pswd
 */
function CreerVendeur($Login, $Pswd){

    //Création de vendeur

    $TypeMembre = 3;

    $connexion = getBD();
    $req = "INSERT INTO membres (Login, Pswd, DateRegister, FKTypeMembre) VALUES ('$Login', '$Pswd', NOW(), '$TypeMembre')";

    $connexion->exec($req);


}

/**
 * @Description Insertion d'un produit dans la table produit
 * @param $Nom
 * @param $Desc
 * @param $Marque
 * @param $PrixUnitaire
 * @param $Quantite
 * @param $TypeProduit
 * @param $nomFichier1
 * @param $nomFichier2
 * @param $nomFichier3
 */
function CreerArticle($Nom, $Desc, $Marque, $PrixUnitaire, $Quantite, $TypeProduit, $nomFichier1, $nomFichier2, $nomFichier3)
{

    //Création d'article

    $connexion = getBD();
    $req = "INSERT INTO produits (Nom, Description, Marque, PrixUnite, DateCreation, Quantite, Image1, Image2, Image3, FKTypeProduit) VALUES ('$Nom', '$Desc', '$Marque', '$PrixUnitaire', NOW(), '$Quantite', '$nomFichier1', '$nomFichier2', '$nomFichier3', '$TypeProduit')";

    $connexion->exec($req);
}

/**
 * @Description Insertion d'une ville dans la table villes
 * @param $Ville
 * @param $NPA
 */
function CreerVille($Ville, $NPA){

    //Création de ville

    $connexion = getBD();
    $req = "INSERT INTO villes (Ville, NPA) VALUES ('$Ville', '$NPA')";

    $connexion->exec($req);

}

/**
 * @Description Insertion d'une adresse dans la table villeparclient
 * @param $Adresse
 * @param $IdVille
 * @param $IdMembre
 */
function CreerAdresse($Adresse, $IdVille, $IdMembre){

    //Création de ville

    $connexion = getBD();
    $req = "INSERT INTO villeparclient (FKMembres, FKVilles, Adresse) VALUES ('$IdMembre', '$IdVille', '$Adresse')";

    $connexion->exec($req);
}

/**
 * @Description Insertion d'une commande dans la table commandes
 * @param $Date
 * @param $PrixFinal
 * @param $IdMembre
 */
function CreerCommande($PrixFinal, $IdMembre, $IdAdresse){

    //Création de commande

    $connexion = getBD();
    $req = "INSERT INTO commandes (Date, PrixFinal, FKMembre, FKAdresse) VALUES (NOW(), '$PrixFinal', '$IdMembre', '$IdAdresse')";

    $connexion->exec($req);
}

/**
 * @Description Insertion des produits lié à une commande dans la table produitparcommande
 * @param $Produit
 * @param $idCommande
 * @param $Qte
 * @param $PrixUnite
 * @param $PrixTotal
 * @param $NouvelleQte
 */
function CreerProduitParCommande($Produit, $idCommande, $Qte, $PrixUnite, $PrixTotal, $NouvelleQte){

    //Création de Produit par commande

    $connexion = getBD();
    $req = "INSERT INTO produitparcommande (FKProduit, FKCommande, QuantiteProduitParCommande, PrixUnite, PrixTotal) VALUES ('$Produit', '$idCommande', '$Qte', '$PrixUnite', '$PrixTotal'); UPDATE produits SET Quantite = '$NouvelleQte' WHERE idProduits = '$Produit'";

    $connexion->exec($req);

}

/**
 * @Description Modification d'un produit dans la table produits
 * @param $Id
 * @param $Nom
 * @param $Marque
 * @param $Desc
 * @param $PrixUnite
 * @param $Qte
 * @param $TypeProduit
 * @param $Image1
 * @param $Image2
 * @param $Image3
 */
function ModificationArticle($Id, $Nom, $Marque, $Desc, $PrixUnite, $Qte, $TypeProduit, $Image1, $Image2, $Image3){

    //Modification d'article
    $connexion = getBD();
    $req = "UPDATE produits SET Nom = '$Nom', Description = '$Desc', Marque = '$Marque', PrixUnite = '$PrixUnite', Quantite = '$Qte', Image1 = '$Image1', Image2 = '$Image2', Image3 = '$Image3', FKTypeProduit = '$TypeProduit' WHERE idProduits = '$Id'";
    $connexion->exec($req);

}

/**
 * @Description Modification d'un produit pour qu'il n'apparaisse plus dans la table produits
 * @param $Id
 */
function SupprimationArticle($Id){
    $connexion = getBD();
    $req = "UPDATE produits SET Actif = 1 WHERE idProduits=$Id";
    $connexion->exec($req);

}

/**
 * @Description Modification d'un produit pour qu'il apparaisse de nouveau dans la table produits
 * @param $Id
 */
function ActiverArticle($Id){
    $connexion = getBD();
    $req = "UPDATE produits SET Actif = 0 WHERE idProduits=$Id";
    $connexion->exec($req);
}

/**
 * @Description Suppression d'un vendeur dans la table membres
 * @param $Id
 */
function SupprimationVendeur($Id){
    $connexion = getBD();
    $req = "DELETE FROM membres WHERE idMembres=$Id";
    $connexion->exec($req);
}

?>


