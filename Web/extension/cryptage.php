<?php
/**
 * Created by PhpStorm.
 * User: MEYLANBenoit
 * Date: 11.05.2018
 * Time: 15:32
 */

class Chiffrement {

// Algorithme utilisé pour le cryptage des blocs
    private static $cipher  = MCRYPT_RIJNDAEL_128;

// Clé de cryptage
    private static $key = "StockElectro";

// Mode opératoire (traitement des blocs)
    private static $mode    = 'cbc';

    public static function crypt($data){
        srand((double)microtime()*1000000);
        $encrypt_key = md5(rand(0,32000));
        $ctr=0;
        $tmp = "";
        for ($i=0;$i<strlen($data);$i++){
            if ($ctr==strlen($encrypt_key)) $ctr=0;
            $tmp.= substr($encrypt_key,$ctr,1) .
                (substr($data,$i,1) ^ substr($encrypt_key,$ctr,1));
            $ctr++;
        }
        return base64_encode($tmp);

        /**        $keyHash = md5(self::$key);
        $key = substr($keyHash, 0, mcrypt_get_key_size(self::$cipher, self::$mode));
        $iv  = substr($keyHash, 0, mcrypt_get_block_size(self::$cipher, self::$mode));

        $data = mcrypt_encrypt(self::$cipher, $key, $data, self::$mode, $iv);

        return base64_encode($data);*/
    }

    public static function decrypt($data){

        $txt = base64_decode($data);
        $tmp = "";
        for ($i=0;$i<strlen($txt);$i++){
            $md5 = substr($txt,$i,1);
            $i++;
            $tmp.= (substr($txt,$i,1) ^ $md5);
        }
        return $tmp;
        /**$keyHash = md5(self::$key);
        $key = substr($keyHash, 0,   mcrypt_get_key_size(self::$cipher, self::$mode) );
        $iv  = substr($keyHash, 0, mcrypt_get_block_size(self::$cipher, self::$mode) );

        $data = base64_decode($data);
        $data = mcrypt_decrypt(self::$cipher, $key, $data, self::$mode, $iv);

        return rtrim($data);*/
    }}


    ?>